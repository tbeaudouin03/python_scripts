

PROFILE=production
BUCKET=s3://media.reflaunt.com

FOLDERS='products/72518 products/74256 products/74259 products/74737 products/75357 products/75359 products/75361 products/75364 products/75409 products/75749 products/76279 products/76629 products/76637 products/76647 products/76649 products/76651 products/76656 products/76660 products/76661 products/76673 products/76676 products/76681 products/76684 products/76691 products/76705 products/76707 products/76712 products/76713 products/76714 products/76717 products/76723 products/76729 products/76730 products/76734 products/76740 products/76755 products/76758 products/76763 products/76769 products/78907 products/79827 products/81592 products/81869 products/82064 products/82507 products/83077 products/83548 products/83552 products/83573 products/83993 products/84036 products/84732 products/84881 products/84886 products/84887 products/84894 products/84898 products/84901 products/84905 products/84906 products/84907 products/84911 products/84948 products/84996 products/85208 products/86168 products/86269 products/88392 products/88985 products/89057 products/89180 products/89693 products/89696 products/89735 products/89743 products/89745 products/89747 products/89763 products/89869 products/89871 products/89879 products/89885 products/89887 products/89890 products/90266 products/90270 products/90273 products/90274 products/90276 products/90287 products/90613 products/90814 products/91754 products/91755 products/91844 products/91845 products/91848 products/92052 products/93135 products/93136 products/93138 products/94556 products/95237 products/95238 products/95241 products/95245 products/95907 products/96098 products/96283 products/97441 products/97445 products/97447 products/98050'
for FOLDER in $FOLDERS
do
echo '---------------'
echo $FOLDER
aws s3 sync $BUCKET/$FOLDER ./$FOLDER/ --profile $PROFILE
done